#!/usr/bin/make -f
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

SHELL := /bin/bash

include /usr/share/GNUstep/debian/config.mk
d_app    := $(CURDIR)/debian/gorm.app
d_dev    := $(CURDIR)/debian/libinterfacebuilder-dev
resdir   := $(GNUSTEP_SYSTEM_APPS)/Gorm.app/Resources
usrshare := /usr/share/GNUstep/Gorm.app
framewrk := /GNUstep/Frameworks/GormCore.framework/Versions/0
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--no-undefined \
				    -Wl,-rpath,/usr/lib/gorm.app
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@

override_dh_auto_build-arch:
	dh_auto_build -- $(optim) $(verbose) \
	  $(shell dpkg-buildflags --export=cmdline)
	convert Applications/Gorm/Images/Gorm.tiff -resize 64x64 Gorm.png

override_dh_auto_build-indep:
# Support "nodoc" in DEB_BUILD_PROFILES and DEB_BUILD_OPTIONS.  If
# present in DEB_BUILD_PROFILES, the gorm.app-doc package is not
# built.  If present only in DEB_BUILD_OPTIONS, the gorm.app-doc
# package is built but it's empty (as per Policy §4.9.1).
ifeq (,$(findstring nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C Documentation
endif

override_dh_clean:
	[ ! -f Documentation/Gorm.info ] || $(MAKE) -C Documentation distclean
	dh_clean Gorm.png debian/gorm.app-doc.install debian/gorm.app-doc.links

override_dh_auto_install-arch:
# Cheat gnustep-make to install the libraries as private.
	$(MAKE) install GNUSTEP_SYSTEM_LIBRARIES=/usr/lib/gorm.app \
	  DESTDIR=$(CURDIR)/debian/tmp
# Reinstall libInterfaceBuilder at /usr/lib.  GormCore is also
# supposed to be a public library but no package is using it, so keep
# it out for now.
	$(RM) $(CURDIR)/debian/tmp/usr/lib/gorm.app/libInterfaceBuilder.so*
	$(MAKE) -C InterfaceBuilder install DESTDIR=$(CURDIR)/debian/tmp
# Ensure /usr/share/GNUstep is present for the gorm.app package,
# otherwise the arch:any build fails.  gsdh_gnustep will populate it
# later in the override_dh_link-arch target.
	mkdir -p $(CURDIR)/debian/tmp/usr/share/GNUstep

override_dh_auto_install-indep:
ifeq (,$(findstring nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C Documentation install DESTDIR=$(CURDIR)/debian/tmp
# Delete files we already ship in /usr/share/doc.
	rm debian/tmp$(GNUSTEP_SYSTEM_DOC)/{ANNOUNCE,INSTALL,NEWS,README}
	cp debian/gorm.app-doc.install.in debian/gorm.app-doc.install
	cp debian/gorm.app-doc.links.in debian/gorm.app-doc.links
endif

override_dh_link-arch:
# Unfortunately we can't use dh_gnustep's standard behavior wrt
# frameworks because the plugins are in GormCore's resource bundle.
	gsdh_gnustep --no-move
	dh_installdirs -pgorm.app $(usrshare)
# Make sure /usr/share/GNUstep/Documentation is deleted from the
# gorm.app package (it ends up there as duplicate in a combined
# arch:any + arch:all build).
	$(RM) -r $(d_app)$(GNUSTEP_SYSTEM_DOC)
	rm $(d_app)$(resdir)/*.desktop
# Move all images to /usr/share in compliance with the FHS.
# We can't move the whole of the app's Resources because the palettes
# are not arch-independent.
	for i in $(d_app)$(resdir)/*.tiff; do \
	  mv $$i $(d_app)$(usrshare) \
	  && dh_link $(usrshare)/$${i##*/} $(resdir)/$${i##*/}; \
	done
# Now move all of palettes' Resources.
	cd $(d_app)$(resdir); \
	  for i in *.palette/Resources; do \
	    mv $$i $(d_app)$(usrshare)/$${i%Resources}; \
	  done
	for i in $(d_app)$(usrshare)/*.palette; do \
	  dh_link $(usrshare)/$${i##*/} $(resdir)/$${i##*/}/Resources; \
	done
# The English.lproj directory contains only .gorm files, but some have
# images inside them.
	mv $(d_app)$(resdir)/English.lproj $(d_app)$(usrshare) \
	  && dh_link $(usrshare)/English.lproj $(resdir)/English.lproj
	chmod -x $(d_app)$(usrshare)/*.tiff
# Move GormCore's headers to /usr/include in the
# libinterfacebuilder-dev package as dh_gnustep would do.  We
# deliberately skip the creation of the redundant symlink
# $(GNUSTEP_SYSTEM_HEADERS)/Frameworks/GormCore.framework/Headers ->
# Versions/0.
	dh_installdirs -plibinterfacebuilder-dev /usr/include/$(DEB_HOST_MULTIARCH)$(framewrk) $(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/Versions
	mv $(d_app)/usr/lib/$(DEB_HOST_MULTIARCH)$(framewrk)/Headers/* \
	  $(d_dev)/usr/include/$(DEB_HOST_MULTIARCH)$(framewrk)
	mv $(d_app)$(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/Headers $(d_dev)$(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework
	mv $(d_app)$(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/Versions/Current $(d_dev)$(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/Versions
# Delete redundant symlinks which end up broken.
	rm $(d_app)/usr/lib/gorm.app/libGormCore.so
	rm $(d_app)$(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/GormCore
	rm $(d_app)$(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/Resources
	rm $(d_app)$(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/libGormCore.so
# Recreate GormCore's Resources symlink avoiding Current which in the
# -dev package.  This is necessary so that it can load its bundles.
	dh_link -pgorm.app \
	  /usr/lib/$(DEB_HOST_MULTIARCH)$(framewrk)/Resources \
	  $(GNUSTEP_SYSTEM_FRAMEWORKS)/GormCore.framework/Resources
	$(RM) -r $(d_app)/usr/lib/$(DEB_HOST_MULTIARCH)$(framewrk)/Headers
	dh_link -plibinterfacebuilder-dev /usr/include/$(DEB_HOST_MULTIARCH)$(framewrk) \
	  /usr/lib/$(DEB_HOST_MULTIARCH)$(framewrk)/Headers
# As we have to retain the plugins in the framework's resource bundle,
# move only images to /usr/share to avoid image-file-in-usr-lib
# complaint from lintian .  As above, English.lproj contains only
# .gorm files but some of them have images so move it altogether.
	dh_installdirs -pgorm.app /usr/share$(framewrk)
	for i in $(d_app)/usr/lib/$(DEB_HOST_MULTIARCH)$(framewrk)/Resources/*.{tiff,lproj}; do \
	  mv $$i $(d_app)/usr/share$(framewrk) \
	  && dh_link /usr/share$(framewrk)/$${i##*/} \
	    /usr/lib/$(DEB_HOST_MULTIARCH)$(framewrk)/Resources/$${i##*/}; \
	done

override_dh_makeshlibs:
# Avoid lintian warnings for private libraries in gorm.app.
	dh_makeshlibs -pgorm.app -n
	dh_makeshlibs -plibinterfacebuilder1 -V
